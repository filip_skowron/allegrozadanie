package org.test;

import io.restassured.RestAssured;
import org.junit.Before;

import static io.restassured.RestAssured.given;


public class Base {

    final String BASE_URI = "https://api.allegro.pl.allegrosandbox.pl";
    final String TOKEN_URL = "https://allegro.pl.allegrosandbox.pl/auth/oauth/token?grant_type=client_credentials";
    final String CLIENT_ID = "e0bcceb01f934fcfbdd4460d6b9ad5bc";
    final String CLIENT_SECRET = "GoSNISqlGEYHQ8cdMobMRmSWHf5QOAiQ31KwLzdYda3m7T0YriNu985DDbkqObxf";
    public static String response;

    @Before
    public void setup() {

        RestAssured.baseURI = BASE_URI;

        response =
                given().auth().basic(CLIENT_ID,CLIENT_SECRET)
                .when()
                    .post(TOKEN_URL)
                .then()
                    .log()
                        .ifError()
                    .statusCode(200)
                        .extract()
                            .path("access_token").toString();

        RestAssured.requestSpecification = given()
                    .header("Authorization", "Bearer " + response)
                    .accept("application/vnd.allegro.public.v1+json");
    }
}
