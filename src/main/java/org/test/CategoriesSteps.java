package org.test;

import io.restassured.RestAssured;
import model.CategoriesResponse;

import java.util.Collections;
import java.util.List;



public class CategoriesSteps {

    public void getIdOfAllegroCategories() {

        List <CategoriesResponse> categoriesResponses = Collections.singletonList(RestAssured
                .given()
                .when()
                    .get("/sale/categories")
                .then()
                    .assertThat()
                        .statusCode(200)
                        .log().all()
                .and()
                    .extract()
                        .response().jsonPath().getObject(".", CategoriesResponse.class));


    }
}

