package org.test;

import io.restassured.RestAssured;
import model.ParametersSupportedByCategoryResponse;

import java.util.Collections;
import java.util.List;


public class ParametersSupportedByCategorySteps {

    public static String getId;

    public void getParametersSupportedById() {

        getId = RestAssured
                .given()
                 .when()
                .get("/sale/categories")
                .then()
                    .log().all()
                    .extract().response().jsonPath().getString("categories.id[0]");

        List <ParametersSupportedByCategoryResponse> parametersResponses = Collections.singletonList(RestAssured
                .given()
                .when()
                    .get("/sale/categories/" + getId + "/parameters")
                .then()
                    .assertThat()
                        .statusCode(200)
                        .log().all()
                .and()
                    .extract()
                        .response().jsonPath().getObject(".", ParametersSupportedByCategoryResponse.class));


    }
}

