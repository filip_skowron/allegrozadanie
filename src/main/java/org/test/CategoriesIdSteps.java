package org.test;


import io.restassured.RestAssured;
import model.CategoriesResponse;

import java.util.Collections;
import java.util.List;


public class CategoriesIdSteps {

    public static String getId;

    public void getCategoryById() {

        getId = RestAssured
                .given()
                 .when()
                .get("/sale/categories")
                .then()
                    .log().all()
                    .extract().response().jsonPath().getString("categories.id[0]");

        System.out.println("Jestem tutaj 1");


        List <CategoriesResponse.Categories> categoriesResponses = Collections.singletonList(RestAssured
                .given()
                .when()
                    .get("/sale/categories/" + getId)
                .then()
                    .assertThat()
                        .statusCode(200)
                        .log().all()
                .and()
                    .extract()
                        .response().jsonPath().getObject(".", CategoriesResponse.Categories.class));
        System.out.println("Jestem tutaj 2");


    }
}

