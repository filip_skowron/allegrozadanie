package model;

import java.util.List;

public class CategoriesResponse {

    private List<Categories> categories;

    public List<Categories> getCategories () {
        return  categories;
    }

    public void setCategories(List<Categories> categories) {
        this.categories = categories;
    }

    public static class Categories {

        private String id;
        private String name;
        private boolean parent;
        private boolean leaf;
        private Options options = null;


        public Options getOptions() {
            return options;
        }

        public void setOptions(Options options) {
            this.options = options;
        }

        public String getId() {
            return id;
        }

        public void setId() {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName() {
            this.name = name;
        }

        public boolean getParent() {
            return parent;
        }

        public void setParent(boolean parent) {
            this.parent = parent;
        }

        public boolean getLeaf() {
            return leaf;
        }

        public void setLeaf(boolean leaf) {
            this.leaf = leaf;
        }
        public  static class Options {

            private boolean advertisement;
            private boolean advertisementPriceOptional;
            private boolean variantsByColorPatternAllowed;
            private boolean offersWithProductPublicationEnabled;
            private boolean productCreationEnabled;
            private boolean customParametersEnabled;

            public boolean getVariantsByColorPatternAllowed() {
                return variantsByColorPatternAllowed;
            }
            public void setVariantsByColorPatternAllowed (boolean variantsByColorPatternAllowed) {
                this.variantsByColorPatternAllowed = variantsByColorPatternAllowed;
            }

            public boolean getadvertisement() {
                return advertisement;
            }
            public void setadvertisement (boolean advertisement) {
                this.advertisement = advertisement;
            }

            public boolean getadvertisementPriceOptional() {
                return advertisementPriceOptional;
            }
            public void setadvertisementPriceOptional (boolean advertisementPriceOptional) {
                this.advertisementPriceOptional = advertisementPriceOptional;
            }

            public boolean getoffersWithProductPublicationEnabled() {
                return advertisementPriceOptional;
            }
            public void setoffersWithProductPublicationEnabled (boolean offersWithProductPublicationEnabled) {
                this.offersWithProductPublicationEnabled = offersWithProductPublicationEnabled;
            }

            public boolean getproductCreationEnabled() {
                return productCreationEnabled;
            }
            public void setproductCreationEnabled (boolean productCreationEnabled) {
                this.productCreationEnabled = productCreationEnabled;
            }

            public boolean getcustomParametersEnabled() {
                return customParametersEnabled;
            }
            public void setcustomParametersEnabled (boolean customParametersEnabled) {
                this.customParametersEnabled = customParametersEnabled;
            }




        }
    }
}
