package model;

import java.util.List;

public class ParametersSupportedByCategoryResponse {

    private List<Parameters> parameters;

    public List<Parameters> getParameters () {
        return  parameters;
    }

    public void setParameters(List<Parameters> parameters) {
        this.parameters = parameters;
    }

    public static class Parameters {

        private String id;
        private String name;
        private String type;
        private boolean required;
        private boolean requiredForProduct;
        private String unit;
        private Options options = null;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getUnit() {
            return unit;
        }

        public void setUnit(String unit) {
            this.unit = unit;
        }

        public boolean isRequiredForProduct() {
            return requiredForProduct;
        }

        public void setRequiredForProduct(boolean requiredForProduct) {
            this.requiredForProduct = requiredForProduct;
        }

        public boolean isRequired() {
            return required;
        }

        public void setRequired(boolean required) {
            this.required = required;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public Options getOptions() {
            return options;
        }

        public void setOptions(Options options) {
            this.options = options;
        }

        public  static class Options {
            public boolean isVariantsAllowed() {
                return variantsAllowed;
            }

            public void setVariantsAllowed(boolean variantsAllowed) {
                this.variantsAllowed = variantsAllowed;
            }

            private boolean variantsAllowed;

            public boolean isVariantsEqual() {
                return variantsEqual;
            }

            public void setVariantsEqual(boolean variantsEqual) {
                this.variantsEqual = variantsEqual;
            }

            private boolean variantsEqual;

            public String getAmbiguousValueId() {
                return ambiguousValueId;
            }

            public void setAmbiguousValueId(String ambiguousValueId) {
                this.ambiguousValueId = ambiguousValueId;
            }

            private String ambiguousValueId;

            public String getDependsOnParameterId() {
                return dependsOnParameterId;
            }

            public void setDependsOnParameterId(String dependsOnParameterId) {
                this.dependsOnParameterId = dependsOnParameterId;
            }
            private RequiredDependsOnValueIds requiredDependsOnValueIds = null;

            public RequiredDependsOnValueIds getRequiredDependsOnValueIds() {
                return requiredDependsOnValueIds;
            }

            public void setRequiredDependsOnValueIds(RequiredDependsOnValueIds requiredDependsOnValueIds) {
                this.requiredDependsOnValueIds = requiredDependsOnValueIds;
            }

            private String dependsOnParameterId;

            private DisplayDependsOnValueIds displayDependsOnValueIds = null;

            public DisplayDependsOnValueIds getDisplayDependsOnValueIds() {
                return displayDependsOnValueIds;
            }

            public void DisplayDependsOnValueIds(DisplayDependsOnValueIds displayDependsOnValueIds) {
                this.displayDependsOnValueIds = displayDependsOnValueIds;
            }

            public boolean isDescribesProduct() {
                return describesProduct;
            }

            public void setDescribesProduct(boolean describesProduct) {
                this.describesProduct = describesProduct;
            }

            private boolean describesProduct;

            public boolean isCustomValuesEnabled() {
                return customValuesEnabled;
            }

            public void setCustomValuesEnabled(boolean customValuesEnabled) {
                this.customValuesEnabled = customValuesEnabled;
            }

            private boolean customValuesEnabled;

            public static class RequiredDependsOnValueIds {

            }

            public static class DisplayDependsOnValueIds {

            }

        }


    }
}